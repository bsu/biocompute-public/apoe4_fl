###############################################################################
#
#   Run DESeq2 Analysis
#
###############################################################################

library('DESeq2')

source("deseq2_functions.r")

# Location of DESeq2 raw count files
#directory <- "location_of_DESeq2_files/"
directory <- getwd()  

###############################################################################
#
#   Build Dataframe
#
###############################################################################

sampleFiles <- grep(".deseq2.txt",list.files(directory),value=TRUE)

# Set Analysis Conditions and Names by Sample
sampleConditions <- c("Control", "Control", "Control", 
                        "FL_E4", "FL_E4", "FL_E4")

sampleNames <- c("Control_1", "Control_2", "Control_3", 
                "Fl_E4_1", "FL_E4_2", "FL_E4_3")                        

sampleTable <- data.frame(sampleName=sampleNames, 
                            fileName=sampleFiles,
                            condition=sampleConditions)

###############################################################################
#
# Run Analysis
#
###############################################################################

ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable=sampleTable,               
                                        directory=directory,
                                        design=~condition)

ddsHTSeqsf<-estimateSizeFactors(ddsHTSeq)

sizeFactors(ddsHTSeqsf)

colData(ddsHTSeqsf)$condition <- relevel(ddsHTSeqsf$condition, ref="Control")

dds <-DESeq(ddsHTSeqsf)

###############################################################################
#
#   Set Common Variables
#
###############################################################################


color = colorRampPalette(rev(brewer.pal(n = 9, name = "Set1")))(24)
hmcol <- colorRampPalette(brewer.pal(9,"Blues"))(100)

vsd <- varianceStabilizingTransformation(dds,blind=TRUE)
rld <- rlogTransformation(dds,blind=TRUE)

t50 <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:50]
t100 <- order(rowMeans(counts(dds,normalized=TRUE)),decreasing=TRUE)[1:100]

# Sample to sample distances of rld transformed data for dendogram
distsRL <- dist(t(assay(rld)))
distMatrix <- as.matrix(distsRL)
rownames(distMatrix) <- colnames(distMatrix) <- with(colData(dds), sampleNames)
hc <- hclust(distsRL)



###############################################################################
#
#   Print Output
#
###############################################################################

mecdf_plot(color,ddsHTSeq, ddsHTSeqsf, sampleNames, 200)
multdens_plot(color,ddsHTSeq, ddsHTSeqsf, sampleNames,200)

hm_all(dds,hmcol, t50)
hm_rld(rld, hmcol, t50)
hm_vsd(vsd, hmcol, t50 )

dend_clust(distMatrix,hmcol, hc)

pca_rld(rld)
pca_vsd(vsd)

diff_exp_anal_function(dds,'FL_E4','Control') 