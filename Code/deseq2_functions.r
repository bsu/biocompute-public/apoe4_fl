###############################################################################
#
#                           Libraries and Packages
#
###############################################################################

library('ggplot2')
library('gplots')
library('geneplotter')
library('pheatmap')
library('RColorBrewer')

###############################################################################
#
#   Density Plot Count for each sample to assess their distributions
#
###############################################################################


#   Multiple Empirical Cummulative Density Functions Plot

mecdf_plot <- function(color, ddsHTSeq, ddsHTSeqsf, sampleNames, limx) {
    
    gtz <- apply(counts(ddsHTSeq),1,function(x){all(x>0)})

    pdf("multiple_empirical_CDF.pdf", width=11,height=7.5)
    
    par(oma=c(1,1,1,1), mar = c(5,5,2,17))

    multiecdf(counts(ddsHTSeqsf, normalized=T)[gtz,], 
                col=color, 
                xlab="Mean Counts", 
                xlim=c(0,limx),
                ylab="Cumulative Probability", 
                main = "CDF of Mean Alignment Counts",
                legend = list("topright", inset=c(-.47,0), 
                    legend=sampleNames, 
                    fill=color, 
                    title="Samples", 
                    bg = "white", 
                xpd=TRUE))
    dev.off()
}

#   Multiple Density Functions Plot

multdens_plot <- function(color, ddsHTSeq, ddsHTSeqsf,sampleNames, limx){

    gtz <- apply(counts(ddsHTSeq),1,function(x){all(x>0)})
    
    pdf("multiple_density.pdf", width=11, height=7.5)

    par(oma=c(1,1,1,1), mar = c(5,5,2,17))

    multidensity(counts(ddsHTSeqsf, normalized=T)[gtz,], 
                col=color, 
                xlab="Mean Counts", 
                xlim=c(0,limx), 
                main = "Probability Density of Mean Alignment Counts ",
                legend = list("topright", inset=c(-.47,0), 
                    legend=sampleNames, 
                    fill=color, 
                    title="Samples", 
                    bg = "white", 
                    xpd=TRUE))
    dev.off()
}


###############################################################################
#
#   Heatmap Plot for highly expressed genes
#
###############################################################################

# Top expressed genes. 

hm_all <- function(dds,hmcol,limit){
    pdf("most_expressed_genes_heatmap_allsamples.pdf", 
        width=9, 
        height=11)

    x <- counts(dds,normalized=TRUE)[limit,]
  
    heatmap.2(x,
              col=hmcol, 
              Rowv=FALSE, 
              Colv=FALSE, 
              scale="none", 
              dendrogram="none",
              trace="none",
              density.info="none",
              margin=c(12,8), 
              lhei=c(2,8), 
              lwid=c(2,4),
              main = "Normalized Counts")
    dev.off()
}

# Regularized log transformation (rld)

hm_rld <- function(rld, hmcol, limit){
    
    pdf("most_expressed_genes_heatmap_rld.pdf", 
        width=9, 
        height=11)

    x <- assay(rld)[limit,]
  
    heatmap.2(x,
              col=hmcol,
              Rowv=FALSE, 
              Colv=FALSE, 
              scale="none",
              dendrogram="none", 
              trace="none",
              density.info="none",
              margin=c(12,8), 
              lhei=c(2,8), 
              lwid=c(2,4),
              main = "Regularized Log Transformation")
    dev.off()
}


# Top Variance stabilizing transformation (vsd)

hm_vsd <- function(vsd, hmcol, limit){
    
    pdf("most_expressed_genes_heatmap3_vsd.pdf", 
        width=9, 
        height=11)
  
    x <- assay(vsd)[limit,]

    heatmap.2(x,
              col=hmcol,
              Rowv=FALSE, 
              Colv=FALSE, 
              scale="none",
              dendrogram="none", 
              trace="none",
              density.info="none",
              margin=c(12,8), 
              lhei=c(2,8), 
              lwid=c(2,4),
              main = "Variance Stabilizing Transformation")
    dev.off()
}


###############################################################################
#
#   Dendogram Plot for Clustering
#
###############################################################################

dend_clust <- function(distMatrix, hmcol, hc){
    
    pdf("deseq2_sample_clustering_allsamples.pdf", 
        width=11, 
        height=11)

    heatmap.2(distMatrix,
                Rowv=as.dendrogram(hc),
                dendrogram = "row",
                symm=TRUE,
                trace="none",
                #tracecol = "red",
                density.info="none",
                col=rev(hmcol),
                margin=c(16,16),
                main = "Euclidean Distance")
    
    dev.off()
}

###############################################################################
#
#   Principal Component Analysis Plots
#
###############################################################################

pca_rld <- function(rld){
    pdf("PCA_Plot_allsamples_rld.pdf", 
        width=11, 
        height=6)
  
    print(plotPCA(rld,intgroup=c("condition")))
    dev.off()
}

pca_vsd <- function(vsd){
    pdf("PCA_Plot_allsamples_vsd.pdf", 
        width=11, 
        height=6)

    print(plotPCA(vsd,intgroup=c("condition")))
    dev.off()
}


###############################################################################
#
#   MA Plot - Bland-Altman plot application
#
###############################################################################
plot_MA <- function(x, A, B){
# Make MA plots of the DE results and print or save to a pdf file.  
    pdf(file= paste(A,"_vs_",B,".pdf",sep=""), 
        width = 9, 
        height = 7.5)

    plotMA(x,
           alpha = 0.05,
           colSig = "blue",
           colNonSig = "gray60",
           ylim=c(-10,10),
           main = "DESeq2 MA Plot",
           xlab = "Mean of Normalized Counts", 
           ylab = expression(paste("Log"[2]," Fold-Change")))
    
    dev.off()
}

###############################################################################
#
#   Differential Expression Analysis
#
###############################################################################

diff_exp_anal_function <- function(dds, A,B) {
  
  #Control or reference library should be listed second (B)!
  ref_A_B <- results(dds, contrast=c("condition", A, B))
  
  #generate results file and add normalized read counts to the results file
  resdata.A_B <- merge(as.data.frame(ref_A_B), 
                                as.data.frame(counts(dds, normalized=TRUE)),by="row.names", 
                                sort=FALSE)
  
  #Look at the file
  head (resdata.A_B)
  
  #Print file
  write.csv(resdata.A_B, 
            file= paste(A, 
                        "_vs_", 
                        B, 
                        ".deseq2.csv", 
                        sep=""))
  
  result <- plot_MA(ref_A_B, A, B)
  
  return(result)
}



