# DESeq2 Analysis

Deseq2 differential expression analysis of 151 amino acid fragment of the APOE4 protein as compared to the full-length APOE4 protein.


## Steps

| Step | Process | Code |
| --- | --- | --- |
| 1. | Quality Check of Sequences | [FastQC](Code/fastqc) |
| 2. | Compile Index | [hisat2-build](Code/compile_index) |
| 3. | Complete Alignment | [hisat2](Code/hisat2) |
| 4. | Complete Counts | [htseq-count](Code/htseq) |
| 5. | Pre-processing | [Custom Script](Code/pre_processing) |
| 6. | DESeq2 Analysis in R |[DESeq2](Code/deseq2.r) |
| 7. | Post-processing | [Custom Script](Code/post_processing) |

## Packages and Software
| Installed | Version | Instructions |
| --- | --- | --- |
| FastQC | v0.11.9 | [Installing FastQC](https://raw.githubusercontent.com/s-andrews/FastQC/master/INSTALL.txt) |
| HISAT2 | v2.2.1 | [Obtaining HISAT2](http://daehwankimlab.github.io/hisat2/manual/) |
| HTSeq| v0.13.5 |[Prequisites and installation](https://htseq.readthedocs.io/en/master/install.html) |
|DESeq2|v1.30.1|[Installation - Bioconductor](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)|

Additionally, R (version 4.0.5) was installed using this [CRAN mirror](https://ftp.osuosl.org/pub/cran/).  Python (version 3.8.5) was also installed. All code was run on Ubuntu 20.04, MacOS 11.2.3, and CentOS 7.

We would like to acknowledge high-performance computing support of the Borah compute cluster (DOI: [10.18122/oit/3/boisestate](https://scholarworks.boisestate.edu/oit/3/)) provided by Boise State University’s Research Computing Department.

## Implementation Details

Numerous R dependencies related to DESeq2 are required including `curl` and `XML`. Additionally, R code contained in the file [deseq2-functions.r](Code/deseq2_functions.r) is used to create the graphs and other output files. This file uses the `ggplot2`, `gplots`, `geneplotter`, `pheatmap`, and `RColorBrewer` libraries in R.

The included code may be run in steps in a terminal window or by aggregation into a shell script. 

## Output

* PDF Graph Output can be located [here](https://gitlab.com/bsu/biocompute-public/apoe4_fl/-/tree/master/Presentation).
* Read Output from DESeq2 can be located [here](https://gitlab.com/bsu/biocompute-public/apoe4_fl/-/tree/master/Data).
* Processed Output can be located [here](https://gitlab.com/bsu/biocompute-public/apoe4_fl/-/tree/master/Analysis).
